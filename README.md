# TP

Pour ce TP nous attendons un rendu écrit et documenter au format de votre choix.
Chaque étape comportant un soleil est à faire et à documenter.

## Objectif et définition

Vous allez devoir monter un SIEM :)

:sun_with_face: Plus sérieusement, nous attendons de vous sur ces 3 jours que vous montiez un SIEM. Alors qu'est -e qu'un SIEM ? Justement c'est à vous de nous le dire, vous devrez nous définir l'acronyme et nous donner une définition. (Et pas un copié-collé :wink:)

## Mise en place

Maintenant il faut en mettre un en place, pour ça vous utiliserez les solutions Suricata et Elastic Stack.
Le but sera d'alimenter les dashboards Kibana que l'on retrouve sur votre solution Elasticsearch.

:sun_with_face: Vous avez une VM à votre disposition, d'ici mercredi nous devons être en capacité de voir une stack Suricata et Elastic Stack fonctionnelle.

:sun_with_face: Maintenant que votre solution est fonctionnelle. Si on fait un curl selon cette syntaxe ```curl google.com``` une **Alert** va remonter dans le dashboard alert, trouver le moyen de ne plus afficher cette alerte lorsqu'on effectue la commande ```curl google.com```.

:sun_with_face: Vous devez nous expliquer la règle suivante : 
```
alert dns any any -> any 53 (msg:"Suspicious – dnslog.cn DNS Query Observed"; flow:stateless; dns_query; content:"dnslog.cn"; fast_pattern:only; threshold:type limit, track by_src, count 1, seconds 3600; classtype:bad-unknown; metadata:created_at 2021-12-10; metadata:ids suricata; priority:2; sid:21003729; rev:1;)
```

:sun_with_face: Maintenant à vous d'écrire 3 règles de détection. Nous vous laissons le choix, cependant ces règles doivent répondre à des problématiques différentes.